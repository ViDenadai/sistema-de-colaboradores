<?php

namespace App\Repositories;

use App\Models\Collaborator;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CollaboratorRepository
 * @package App\Repositories
 * @version October 3, 2018, 2:50 pm UTC
 *
 * @method Collaborator findWithoutFail($id, $columns = ['*'])
 * @method Collaborator find($id, $columns = ['*'])
 * @method Collaborator first($columns = ['*'])
*/
class CollaboratorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'company_id',
        'name',
        'address',
        'phone',
        'photo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Collaborator::class;
    }
}
