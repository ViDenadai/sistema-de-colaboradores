<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Collaborator
 * @package App\Models
 * @version October 3, 2018, 2:50 pm UTC
 *
 * @property \App\Models\ Company company
 * @property integer company_id
 * @property string name
 * @property string address
 * @property string phone
 * @property string photo
 */
class Collaborator extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;

    public $table = 'collaborators';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'company_id',
        'name',
        'address',
        'phone',
        'photo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'company_id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'photo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'company_id' => 'required',
        'name' => 'required',
        'address' => 'required',
        'phone' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\ Company::class, ' company_id', ' id');
    }
}
