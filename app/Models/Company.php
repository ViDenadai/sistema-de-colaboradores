<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Company
 * @package App\Models
 * @version October 3, 2018, 2:49 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Collaborator
 * @property string name
 * @property string address
 * @property string phone
 * @property string photo
 */
class Company extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;

    public $table = 'companies';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'address',
        'phone',
        'photo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'photo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'address' => 'required',
        'phone' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function collaborators()
    {
        return $this->hasMany(\App\Models\Collaborator::class, 'company_id');
    }
    
}
