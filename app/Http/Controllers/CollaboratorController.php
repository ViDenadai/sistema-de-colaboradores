<?php

namespace App\Http\Controllers;

use App\DataTables\CollaboratorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCollaboratorRequest;
use App\Http\Requests\UpdateCollaboratorRequest;
use App\Repositories\CollaboratorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollaboratorController extends AppBaseController
{
    /** @var  CollaboratorRepository */
    private $collaboratorRepository;

    public function __construct(CollaboratorRepository $collaboratorRepo)
    {
        $this->collaboratorRepository = $collaboratorRepo;
    }

    /**
     * Display a listing of the Collaborator.
     *
     * @param CollaboratorDataTable $collaboratorDataTable
     * @return Response
     */
    public function index(CollaboratorDataTable $collaboratorDataTable, Request $request)
    {
        // Id da empresa
        $companyId = $request->id;
        
        // Nome da empresa
        $companyName = $request->name;

        // Variável que me retorna todos os colaboradores da empresa escolhida
        $collaborators = DB::table('collaborators')->where([
            ['company_id', '=', $companyId],
            ['deleted_at', '=', NULL],        
        ])->get();
                
        return view('collaborators.index', compact('collaborators', 'companyName', 'companyId'));
    }

    /**
     * Show the form for creating a new Collaborator.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        // Id da empresa
        $companyId = $request->id;

        // Nome da empresa
        $companyName = $request->name;
        return view('collaborators.create', compact('companyName', 'companyId'));
    }

    /**
     * Store a newly created Collaborator in storage.
     *
     * @param CreateCollaboratorRequest $request
     *
     * @return Response
     */
    public function store(CreateCollaboratorRequest $request)
    {
        $input = $request->all();
        $collaborator = $this->collaboratorRepository->create($input);

        Flash::success('Colaborador cadastrado com sucesso.');

        // Redirecionamento para a página da empresa com as
        // informações necessárias
        return redirect(action('CollaboratorController@index', 
                            ['id' => $input['company_id'] , 
                            'name' => $input['company_name']]));
    }

    /**
     * Display the specified Collaborator.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $collaborator = $this->collaboratorRepository->findWithoutFail($id);

        if (empty($collaborator)) {
            Flash::error('Collaborator not found');

            return redirect(route('collaborators.index'));
        }

        return view('collaborators.show')->with('collaborator', $collaborator);
    }

    /**
     * Show the form for editing the specified Collaborator.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $collaborator = $this->collaboratorRepository->findWithoutFail($id);

        // Nome da empresa
        $companyName = DB::table('companies')->select('name')->where('id', $collaborator['company_id'])->get();
        $companyName = $companyName[0]->name;

        if (empty($collaborator)) {
            Flash::error('Colaborador não encontrado.');

            return redirect(action('CollaboratorController@index', 
                                ['id' => $collaborator['id'], 
                                'name' => $companyName]));
        }
    
        return view('collaborators.edit', compact('collaborator', 'companyName'));
    }

    /**
     * Update the specified Collaborator in storage.
     *
     * @param  int              $id
     * @param UpdateCollaboratorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCollaboratorRequest $request)
    {
        $collaborator = $this->collaboratorRepository->findWithoutFail($id);

        if (empty($collaborator)) {
            Flash::error('Collaborator not found');

            return redirect(route('collaborators.index'));
        }

        $collaborator = $this->collaboratorRepository->update($request->all(), $id);

        Flash::success('Collaborator updated successfully.');

        return redirect(route('collaborators.index'));
    }

    /**
     * Remove the specified Collaborator from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $collaborator = $this->collaboratorRepository->findWithoutFail($id);

        if (empty($collaborator)) {
            Flash::error('Collaborator not found');

            return redirect(route('collaborators.index'));
        }

        $this->collaboratorRepository->delete($id);

        Flash::success('Collaborator deleted successfully.');

        return redirect(route('collaborators.index'));
    }

    // Função de update dos dados de colaborador
    public function updateInfo(Request $request)
    {
        // Recuperação das variáveis do POST
        $name = $request->post('name');
        $address = $request->post('address');
        $phone = $request->post('phone');
        $photo = $request->post('photo');
        $id = $request->post('id');
        $companyId = $request->post('company_id');
        $companyName = $request->post('company_name');

        // Busca o colaborador pelo id
        $collaborator = $this->collaboratorRepository->findWithoutFail($id);

        if (empty($collaborator)) {
            Flash::error('Colaborador não encontrado.');

            return redirect(action('CollaboratorController@index', 
                                ['id' => $companyId, 
                                'name' => $companyName]));
        }

        // Se uma nova foto foi adicionada o nome dela
        // é adicionado na tabela collaborator, caso contrário,
        // o nome da antiga foto é posto no update
        if(empty($photo)) {
            $photo = $collaborator->photo;
        }

        // Formação do vetor com as novas informções
        // para o update (isso é feito para a reutilização
        // da função update da classe collaboratorRepository)
        $newInfo = array (
            "company_id" => $companyId,
            "name" => $name,
            "address" => $address,
            "phone" => $phone,
            "photo" => $photo,            
        );

        // Atualização das informações
        $this->collaboratorRepository->update($newInfo, $id);
        
        // Mensagem de sucesso
        Flash::success('Informações do colaborador foram atualizadas com sucesso.');

        return redirect(action('CollaboratorController@index', 
                            ['id' => $companyId, 
                            'name' => $companyName]));        
    }

    // Função para deletar um colaborador da base de dados
    public function delete(Request $request)
    {
        // Recuperação das variáveis do POST
        $id = $request->post('id');
        $companyId = $request->post('company_id');
        $companyName = $request->post('company_name');

        // Busca o colaborador pelo id
        $collaborator = $this->collaboratorRepository->findWithoutFail($id);

        if (empty($collaborator)) {
            Flash::error('Colaborador não encontrado.');

            return redirect(action('CollaboratorController@index', 
                                ['id' => $companyId, 
                                'name' => $companyName]));
        }

        $this->collaboratorRepository->delete($id);

        Flash::success('Colaborador removido com sucesso.');

        return redirect(action('CollaboratorController@index',  
                            ['id' => $companyId, 
                            'name' => $companyName]));       
    }
}
