<?php

namespace App\Http\Controllers;

use App\DataTables\CompanyDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Repositories\CompanyRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends AppBaseController
{
    /** @var  CompanyRepository */
    private $companyRepository;

    public function __construct(CompanyRepository $companyRepo)
    {
        $this->companyRepository = $companyRepo;
    }

    /**
     * Display a listing of the Company.
     *
     * @param CompanyDataTable $companyDataTable
     * @return Response
     */
    public function index(CompanyDataTable $companyDataTable)
    {
        // Variável que me retorna a view todas as companhias existentes
        $companies = Company::all();

        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new Company.
     *
     * @return Response
     */
    public function create()
    {        
        return view('companies.create');
    }

    /**
     * Store a newly created Company in storage.
     *
     * @param CreateCompanyRequest $request
     *
     * @return Response
     */
    public function store(CreateCompanyRequest $request)
    {
        $input = $request->all();        
        $company = $this->companyRepository->create($input);

        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {                            
            // É gerado um nome aleatório para a foto baseado no timestamp atual 
            $name = uniqid(date('HisYmd')); 
            $nameWithExtension = $name . '.' . $request->photo->getClientOriginalExtension();

            // [função usingName faz a troca do nome do arquivo; função usingFileName faz a troca do nome do arquivo com a extensão]
            $company->addMedia($request->file('photo'))->usingName($name)->usingFileName($nameWithExtension)->toMediaCollection('photos');
        }

        Flash::success('Empresa cadastrada com sucesso.');

        return redirect(route('companies.index'));
    }

    /**
     * Display the specified Company.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            Flash::error('Company not found');

            return redirect(route('companies.index'));
        }

        return view('companies.show')->with('company', $company);
    }

    /**
     * Show the form for editing the specified Company.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            Flash::error('Company not found');

            return redirect(route('companies.index'));
        }

        return view('companies.edit')->with('company', $company);
    }

    /**
     * Update the specified Company in storage.
     *
     * @param  int              $id
     * @param UpdateCompanyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompanyRequest $request)
    {
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            Flash::error('Company not found');

            return redirect(route('companies.index'));
        }

        $company = $this->companyRepository->update($request->all(), $id);

        Flash::success('Company updated successfully.');

        return redirect(route('companies.index'));
    }

    /**
     * Remove the specified Company from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            Flash::error('Empresa não encontrada.');

            return redirect(route('companies.index'));
        }

        $this->companyRepository->delete($id);

        Flash::success('Company deleted successfully.');

        return redirect(route('companies.index'));
    }

    // Função de update dos dados de empresa
    public function updateInfo(Request $request)
    {
        // Recuperação das variáveis do POST
        $name = $request->post('name');
        $address = $request->post('address');
        $phone = $request->post('phone');
        $photo = $request->post('photo');
        $id = $request->post('id');

        // Busca a empresa pelo id
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            Flash::error('Empresa não encontrada.');

            return redirect(route('companies.index'));
        }

        // Se uma nova foto foi adicionada o nome dela
        // é adicionado na tabela company, caso contrário,
        // o nome da antiga foto é posto no update
        if(empty($photo)) {
            $photo = $company->photo;
        }

        // Formação do vetor com as novas informções
        // para o update (isso é feito para a reutilização
        // da função update da classe companyRepository)
        $newInfo = array (
            "name" => $name,
            "address" => $address,
            "phone" => $phone,
            "photo" => $photo,            
        );

        // Atualização das informações
        $this->companyRepository->update($newInfo, $id);
        
        // Mensagem de sucesso
        Flash::success('Informações da empresa foram atualizadas com sucesso.');

        return redirect(route('companies.index'));        
    }

    // Função para deletar uma empresa da base de dados
    public function delete(Request $request)
    {
        // Recuperação das variáveis do POST
        $id = $request->post('id');

        // Busca a empresa pelo id
        $company = $this->companyRepository->findWithoutFail($id);

        if (empty($company)) {
            Flash::error('Empresa não encontrada.');

            return redirect(route('companies.index'));
        }

        // Remoção dos colaboradores ligados à esta empresa;
        $collaboratorsId = DB::table('collaborators')->select('id')->where('company_id', $id)->get();
        foreach($collaboratorsId as $collaboratorId) {
            DB::table('collaborators')->delete()->where('company_id', $collaboratorId->id);
        }

        // Remoção de empresas
        $this->companyRepository->delete($id);                

        Flash::success('Empresa removida com sucesso.');

        return redirect(route('companies.index'));       
    }
}
