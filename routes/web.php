<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('companies', 'CompanyController');

Route::resource('collaborators', 'CollaboratorController');

// Rota do update, mais elaborado, de empresa
Route::patch('companies', 'CompanyController@updateInfo');

// Rota do delete de empresa
Route::delete('companies', 'CompanyController@delete');

// Rota do update, mais elaborado, de colaborador
Route::patch('collaborators', 'CollaboratorController@updateInfo');

// Rota do delete de colaborador
Route::delete('collaborators', 'CollaboratorController@delete');
