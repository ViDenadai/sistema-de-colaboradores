@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Colaborador da empresa {!! $companyName !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <!-- Form de alteração de informações do colaborador -->
               <form method="post" action="{!! action('CollaboratorController@updateInfo'); !!} ">
                    <!-- Método HTTP utilizado -->
                    <input type="hidden" name="_method" value="PATCH">

                    <!-- CSRF Token (Necessário para a requisição funcionar) -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <!-- Id do usuário -->
                    <input type="hidden" name="id" value="{!! $collaborator['id'] !!}">

                    <!-- Id da empresa -->
                    <input type="hidden" name="company_id" value="{!! $collaborator['company_id'] !!}">

                    <!-- Nome da empresa -->
                    <input type="hidden" name="company_name" value="{!! $companyName !!}">

                    <!-- File -->
                    <!-- <input type="hidden" name="_file" value="true"> -->

                    <!-- Nome do colaborador -->
                    <div class="form-group">
                        <label for="inputNameCollaborator">Nome: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputNameCollaborator" 
                            name="name"
                            value={!! $collaborator["name"] !!}
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Endereço do colaborador -->
                    <div class="form-group">
                        <label for="inputAddressCollaborator">Endereço: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputAddressCollaborator"
                            name="address"
                            value={!! $collaborator["address"] !!}
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>
                    
                    <!-- Telefone do colaborador -->
                    <div class="form-group">
                        <label for="inputPhoneCollaborator">Telefone: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputPhoneCollaborator"
                            name="phone"
                            value={!! $collaborator["phone"] !!}
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Foto do colaborador -->
                    <div class="form-group">
                        <!-- Caso exista alguma foto no sistema o nome dela é visto,
                        caso contrário, uma mensagem é exibida -->
                        <label for="inputPhotoCollaborator">Foto: </label>
                        @if ($collaborator['photo'] != NULL)
                            <span class="text-warning padding-top-sm">
                                <br>
                                Foto existente: {!! $collaborator['photo'] !!}
                            </span>
                        @else
                            <span class="text-danger padding-top-sm">
                                <br>
                                Não há nenhuma foto na plataforma
                            </span>
                        @endif
                        <input type="file" 
                            class="form-control" 
                            id="inputPhotoCollaborator"
                            name="photo">
                    </div>
                    
                    <div class="text-right">
                        <!-- Botão de cancelar -->
                        <a href="{!! action('CollaboratorController@index', 
                                ['id' => $collaborator['company_id'], 'name' => $companyName]); 
                            !!}"
                            class="btn btn-default">
                            Cancelar
                        </a>

                        <!-- Botão de enviar -->
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>                                                
                </form>
           </div>
       </div>
   </div>
@endsection