@section('css')
    @include('layouts.datatables_css')
@endsection

@foreach ($collaborators as $collaborator)
    <!-- InfoBox contendo as informações dos colaboradores  -->
    <div class="col-lg-4 col-md-8 col-sm-8 col-xs-12">
        <div class="info-box">
            <div class="box-header">
                <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">                    
                    <form method="post" action="{!! action('CollaboratorController@delete'); !!}" >
                        <a href={!! 'http://127.0.0.1:8000/collaborators/' . $collaborator->id . '/edit' !!} 
                            class="btn btn-default btn-xs">
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>

                        <!-- Método HTTP utilizado -->
                        <input type="hidden" name="_method" value="DELETE">

                        <!-- CSRF Token (Necessário para a requisição funcionar) -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <!-- Id do usuário -->
                        <input type="hidden" name="id" value="{!! $collaborator->id !!}">

                        <!-- Id da empresa -->
                        <input type="hidden" name="company_id" value="{!! $collaborator->company_id !!}">
                        
                        <!-- Nome da empresa -->
                        <input type="hidden" name="company_name" value="{!! $companyName !!}">

                        <button type="submit"
                            class="btn btn-danger btn-xs" 
                            onclick="return confirm('Você tem certeza?')">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                    </form>                                
                </div>                
            </div>
            <span class="info-box-icon bg-aqua">
                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" 
                    alt="Foto colaborador">
            </span>

            <div class="info-box-content">                                
                <!-- Nome da empresa -->
                <h3>{!! $collaborator->name !!}</h3>

                <!-- Endereço da empresa -->
                <span>Endereço: {!! $collaborator->address !!}<br></span>

                <!-- Telefone da empresa -->
                <span>Telefone: {!! $collaborator->phone !!}<br></span>  
            </div>
        </div>
    </div>
@endforeach

@section('scripts')
    @include('layouts.datatables_js')
@endsection