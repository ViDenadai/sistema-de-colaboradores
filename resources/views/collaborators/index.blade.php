@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Colaboradores da Empresa {!! $companyName !!}</h1>
        <h1 class="pull-right">
            <a  class="btn btn-primary pull-right" 
                style="margin-top: -10px;margin-bottom: 5px" 
                href="{!! action('CollaboratorController@create', 
                        ['id' => $companyId , 'name' => $companyName ]); 
                    !!}">
                <i class="fa fa-plus" style="font-size:18px color: rgb(0,255,0)"></i>
            </a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <!-- Container que exibe os colaboradores existentes -->
        <div class="row padding-xl padding-xs padding-sm padding-md padding-lg">
            @include('collaborators.table')                
        </div>

        <div class="text-center">
        
        </div>
    </div>
@endsection

