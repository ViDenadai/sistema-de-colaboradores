@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Colaborador da empresa {!! $companyName !!}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <!-- Formulário para a adição de um novo colaborador (enctype é necessário pois há upĺoad de arquivo) -->
                <form method="post" action="{!! action('CollaboratorController@store') !!} " enctype="multipart/form-data">
                    <!-- CSRF Token (Necessário para a requisição funcionar) -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <!-- File -->
                    <!-- <input type="hidden" name="_file" value="true"> -->

                    <!-- Id da empresa -->
                    <input type="hidden" name="company_id" value="{!! $companyId !!}"/>

                    <!-- Nome da empresa -->
                    <input type="hidden" name="company_name" value="{!! $companyName !!}"/>

                    <!-- Nome do colaborador -->
                    <div class="form-group">
                        <label for="inputNameCollaborator">Nome: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputNameCollaborator" 
                            name="name"
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Endereço do colaborador-->
                    <div class="form-group">
                        <label for="inputAddressCollaborator">Endereço: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputAddressCollaborator"
                            name="address"
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>
                    
                    <!-- Telefone do colaborador -->
                    <div class="form-group">
                        <label for="inputPhoneCollaborator">Telefone: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputPhoneCollaborator"
                            name="phone"
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Foto do colaborador -->
                    <div class="form-group">
                        <label for="inputPhotoCollaborator">Foto: </label>
                        <input type="file" 
                            class="form-control" 
                            id="inputPhotoCollaborator"
                            name="photo">
                    </div>
                    
                    <div class="text-right">
                        <!-- Botão de cancelar -->
                        <a  href="{!! action('CollaboratorController@index', 
                                ['id' => $companyId , 'name' => $companyName ]); 
                            !!}"
                            class="btn btn-default">
                            Cancelar
                        </a>

                        <!-- Botão de enviar -->
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>                                                
                </form>
            </div>
        </div>
    </div>
@endsection
