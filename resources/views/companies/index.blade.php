@extends('layouts.app')

<!-- Seção de minhas empresas -->
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Minhas Empresas</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('companies.create') !!}">
                <i class="fa fa-plus" style="font-size:18px color: rgb(0,255,0)"></i>
            </a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <!-- Container que exibe as empresas existentes -->
        <div class="row padding-xl padding-xs padding-sm padding-md padding-lg">
            @include('companies.table')                
        </div>
        
        <div class="text-center">
        
        </div>
    </div>
@endsection

