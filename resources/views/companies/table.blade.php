@section('css')
    @include('layouts.datatables_css')
@endsection

@foreach ($companies as $company)
    <!-- InfoBox contendo as informações da empresa  -->
    <div class="col-lg-4 col-md-8 col-sm-8 col-xs-12">
        <div class="info-box">
            <div class="box-header">
                <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">                    
                    <form method="post" action="{!! action('CompanyController@delete'); !!}" >
                        <a href={!! "http://127.0.0.1:8000/companies/" . $company->id . "/edit" !!} 
                            class="btn btn-default btn-xs">
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>

                        <!-- Método HTTP utilizado -->
                        <input type="hidden" name="_method" value="DELETE">

                        <!-- CSRF Token (Necessário para a requisição funcionar) -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <!-- Id do usuário -->
                        <input type="hidden" name="id" value="{!! $company->id !!}">

                        <button type="submit"
                            class="btn btn-danger btn-xs" 
                            onclick="return confirm('Você tem certeza?')">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                    </form>                                
                </div>                
            </div>
            <span class="info-box-icon bg-aqua">
                @if ($company->getFirstMedia('photos') != NULL)
                    <img src={!! $company->getFirstMedia('photos')->getFullUrl() !!} width="150" >  
                @else
                    <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" 
                        alt="Foto empresa">
                @endif                  
            </span>

            <div class="info-box-content">                                
            
            <!-- Nome da empresa -->
                <h3>{!! $company->name !!}</h3>

                <!-- Endereço da empresa -->
                <span>Endereço: {!! $company->address !!}<br></span>

                <!-- Telefone da empresa -->
                <span>Telefone: {!! $company->phone !!}<br></span>  
            </div>

            <div class="box-footer text-center bg-gray">
                <!-- Link para listar os colaboradores -->
                <a  href="{!! action('CollaboratorController@index', 
                            ['id' => $company['id'] , 'name' => $company['name'] ]); 
                        !!}" class="small-box-footer">
                    Colaboradores <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
@endforeach

@section('scripts')
    @include('layouts.datatables_js')
@endsection