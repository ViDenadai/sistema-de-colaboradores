@extends('layouts.app')

@section('css')
    @include('layouts.margin_css')
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Empresa
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
                <!-- Form de alteração de informações da empresa -->
                <form method="post" action="{!! action('CompanyController@updateInfo'); !!} ">
                    <!-- Método HTTP utilizado -->
                    <input type="hidden" name="_method" value="PATCH">

                    <!-- CSRF Token (Necessário para a requisição funcionar) -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <!-- Id do usuário -->
                    <input type="hidden" name="id" value="{!! $company['id'] !!}">

                    <!-- File -->
                    <!-- <input type="hidden" name="_file" value="true"> -->

                    <!-- Nome da empresa -->
                    <div class="form-group">
                        <label for="inputNameCompany">Nome: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputNameCompany" 
                            name="name"
                            value={!! $company["name"] !!}
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Endereço da empresa -->
                    <div class="form-group">
                        <label for="inputAddressCompany">Endereço: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputAddressCompany"
                            name="address"
                            value={!! $company["address"] !!}
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>
                    
                    <!-- Telefone da empresa -->
                    <div class="form-group">
                        <label for="inputPhoneCompany">Telefone: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputPhoneCompany"
                            name="phone"
                            value={!! $company["phone"] !!}
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Foto da empresa -->
                    <div class="form-group">
                        <!-- Caso exista alguma foto no sistema o nome dela é visto,
                        caso contrário, uma mensagem é exibida -->
                        <label for="inputPhotoCompany">Foto: </label>
                        @if ($company['photo'] != NULL)
                            <span class="text-warning padding-top-sm">
                                <br>
                                Foto existente: {!! $company['photo'] !!}
                            </span>
                        @else
                            <span class="text-danger padding-top-sm">
                                <br>
                                Não há nenhuma foto na plataforma
                            </span>
                        @endif
                        <input type="file" 
                            class="form-control" 
                            id="inputPhotoCompany"
                            name="photo">
                    </div>
                    
                    <div class="text-right">
                        <!-- Botão de cancelar -->
                        <a href="http://127.0.0.1:8000/companies/"
                            class="btn btn-default">
                            Cancelar
                        </a>

                        <!-- Botão de enviar -->
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>                                                
                </form>               
           </div>
       </div>
   </div>
@endsection