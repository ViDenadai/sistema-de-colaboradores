@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Empresa
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <!-- Formulário para a adição de uma nova empresa (enctype é necessário pois há upĺoad de arquivo) -->
                <form method="post" action="{!! action('CompanyController@store') !!} " enctype="multipart/form-data">
                    <!-- CSRF Token (Necessário para a requisição funcionar) -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <!-- File -->
                    <!-- <input type="hidden" name="_file" value="true"> -->

                    <!-- Nome da empresa -->
                    <div class="form-group">
                        <label for="inputNameCompany">Nome: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputNameCompany" 
                            name="name"
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Endereço da empresa -->
                    <div class="form-group">
                        <label for="inputAddressCompany">Endereço: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputAddressCompany"
                            name="address"
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>
                    
                    <!-- Telefone da empresa -->
                    <div class="form-group">
                        <label for="inputPhoneCompany">Telefone: </label>
                        <input type="text" 
                            class="form-control" 
                            id="inputPhoneCompany"
                            name="phone"
                            required=""
                            oninvalid="this.setCustomValidity('Preencha este campo.')"
                            oninput="setCustomValidity('')">
                    </div>

                    <!-- Foto da empresa -->
                    <div class="form-group">
                        <label for="inputPhotoCompany">Foto: </label>
                        <input type="file" 
                            class="form-control" 
                            id="inputPhotoCompany"
                            name="photo">
                    </div>
                    
                    <div class="text-right">
                        <!-- Botão de cancelar -->
                        <a href="http://127.0.0.1:8000/companies/"
                            class="btn btn-default">
                            Cancelar
                        </a>

                        <!-- Botão de enviar -->
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>                                                
                </form>     
            </div>
        </div>
    </div>
@endsection
