<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Primeira população das tabelas users, companies e collaborators
        $this->call(FirstSeeder::class);
    }
}
