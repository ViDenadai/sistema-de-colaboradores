<?php

use Illuminate\Database\Seeder;

// Seeder da tabela Collaborators
class FirstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Popula a tabela usuário com 2 usuários
        DB::table('users')->insert([            
            'name' => 'victor',
            'email' => 'victor@gmail.com',
            'password' => bcrypt('123456'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([            
            'name' => 'victor',
            'email' => 'victor@pandoapps.com.br',
            'password' => bcrypt('123456'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        // Popula a tabela com 10 empresas aleatórias
        for($i = 1; $i <= 10; $i++) {
            DB::table('companies')->insert([            
                'name' => str_random(10),
                'address' => str_random(10),
                'phone' => rand(1111111111,9999999999).'',
                'photo' => str_random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }  
        
        // Popula a tabela com 10 colaboradores aleatórios
        for($i = 1; $i <= 10; $i++) {
            DB::table('collaborators')->insert([            
                'company_id' => rand(1,10),
                'name' => str_random(10),
                'address' => str_random(10),
                'phone' => rand(1111111111,9999999999).'',
                'photo' => str_random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }        
    }
}
